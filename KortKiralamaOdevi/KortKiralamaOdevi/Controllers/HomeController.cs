﻿using KortKiralamaOdevi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KortKiralamaOdevi.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var bugun = DateTime.Now.DayOfWeek;
                var listelenecekler = db.Kortlar.Select(x => new AnasayfaViewModel
                {
                    ID = x.ID,
                    KortAdi = x.KortAdi,
                    ResimUrl = x.ResimUrl,
                    Ucret = x.GunlukKortUcretleri.Where(gunlukUcret => gunlukUcret.Gun == bugun).Select(gucret=>gucret.Ucret).FirstOrDefault()
                }).ToList();
                return View(listelenecekler);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            var user = GetLoginedUser();

            return View();
        }
       
        public ActionResult Contact()
        {
            var user = GetLoginedUser();

            return Json(user,JsonRequestBehavior.AllowGet);
        }
    }
}