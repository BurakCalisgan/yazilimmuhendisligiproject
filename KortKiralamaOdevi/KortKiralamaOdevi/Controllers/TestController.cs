﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KortKiralamaOdevi.Models;
using KortKiralamaOdevi.Models.dbEntities;

namespace KortKiralamaOdevi.Controllers
{
    
    public class TestController : BaseController
    {
        public ActionResult Index()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var item = db.Users.FirstOrDefault();
                var listelenecekler = db.Kortlar.Select(x => new
                {
                    x.ID,
                    x.KortAdi,
                    x.ResimUrl,
                    Ucret = x.GunlukKortUcretleri.First(gunlukUcret => gunlukUcret.Gun == DateTime.Now.DayOfWeek).Ucret
                });
                return View(item);
            }
        }
        [Authorize]
        public ActionResult LoginOlmadanGireme()
        {
            return Json("");
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AdminOlmadanGireme()
        {
            var girisYapanKullanici = GetLoginedUser();
            return Json(girisYapanKullanici,JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult RezervasyonVerisiDoldur()
        {
            using (ApplicationDbContext context=new ApplicationDbContext())
            {
                Random rnd = new Random();
                var tumRezervasyonlar = context.Rezervasyonlar.ToList();
                foreach (var rezervasyon in tumRezervasyonlar)
                    context.Rezervasyonlar.Remove(rezervasyon);


                context.SaveChanges();
                var userId = context.Users.FirstOrDefault().Id;
                var kortId = context.Kortlar.FirstOrDefault().ID;
                for (int i = 0; i < 7; i++)
                {
                    Odeme odeme = new Odeme();
                    odeme.OdenecekTutar = rnd.Next(0, 12);
                    odeme.OdendiMi = rnd.Next(0, 999) % 2 == 1;
                    context.Odeme.Add(odeme);
                    context.SaveChanges();


                    Rezervasyon rez = new Rezervasyon();
                    DateTime baslangic = DateTime.Now.AddDays(i);

                    TimeSpan ts = new TimeSpan(rnd.Next(9, 16), rnd.Next(0, 999) % 2 == 1 ? 30 : 0, 0);
                    baslangic = baslangic.Date + ts;
                    rez.BaslangicTarihi = baslangic;
                    rez.BitisTarihi = rez.BaslangicTarihi.AddHours(1);

                    rez.FKUyeID = userId;
                    rez.FKOdemeID = odeme.ID;
                    rez.EnumRezervasyonDurumu = EnumRezervasyonDurumu.Aktif;
                    rez.FKKortID = kortId;
                    context.Rezervasyonlar.Add(rez);
                    context.SaveChanges();

                }

                return RedirectToAction("Index", "Dashboard", new {Area = "admin"});

            }
        }
    }
}