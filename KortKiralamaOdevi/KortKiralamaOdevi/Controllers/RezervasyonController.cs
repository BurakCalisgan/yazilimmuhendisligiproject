﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KortKiralamaOdevi.Areas.admin.Models;
using KortKiralamaOdevi.Controllers;
using KortKiralamaOdevi.Models;
using KortKiralamaOdevi.Models.dbEntities;
using Microsoft.AspNet.Identity.Owin;

namespace KortKiralamaOdevi.Controllers
{
    public class RezervasyonController : BaseController
    {
        RezervasyonBusiness rezervasyonBusiness = new RezervasyonBusiness();
        public ActionResult Index(int kortId)
        {
            ViewBag.NormalSayfa = true;
            if (GetLoginedUser() != null && User.IsInRole("Admin"))
                ViewBag.NormalSayfa = false;
            

            var liste = rezervasyonBusiness.GetirRezervasyonBilgileriIndexViewModel(kortId);
            return View(liste);
        }

        public ActionResult _RezervasyonDetay(int rezervasyonId)
        {
            ViewBag.NormalSayfa = true;
            if (GetLoginedUser() != null && User.IsInRole("Admin"))
                ViewBag.NormalSayfa = false;
            var liste = rezervasyonBusiness.GetirRezevasyonDetayBilgileri(rezervasyonId);
            return View(liste);
        }

        public ActionResult RezervasyonYap()
        {
            using (ApplicationDbContext db =new ApplicationDbContext())
            {
                var kortlar = db.Kortlar.Select(x => new
                {
                    x.ID,
                    x.KortAdi
                }).ToList();
                ViewBag.Kortlar = new SelectList(kortlar, "ID", "KortAdi",kortlar.FirstOrDefault().ID);
            }
            return View();
        }
        [HttpPost]
        public ActionResult RezervasyonYap(RezervasyonYapViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Hata = "";
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var kortlar = db.Kortlar.Select(x => new
                    {
                        x.ID,
                        x.KortAdi
                    }).ToList();
                    ViewBag.Kortlar = new SelectList(kortlar, "ID", "KortAdi", model.FKKortID);
                }
                return View(model);
            }
            if (GetLoginedUser()==null &&(string.IsNullOrEmpty(model.Ad)||string.IsNullOrEmpty(model.Soyad)||string.IsNullOrEmpty(model.TelNo)))
            {
                ViewBag.Hata = "Ad soyad telefon no dolu olmalıdır";
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var kortlar = db.Kortlar.Select(x => new
                    {
                        x.ID,
                        x.KortAdi
                    }).ToList();
                    ViewBag.Kortlar = new SelectList(kortlar, "ID", "KortAdi", model.FKKortID);
                }
                return View(model);
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                double ucret;
                var gucret = db.KortGunlukUcretler.FirstOrDefault(x => x.FKKortID == model.FKKortID &&
                                                                       x.Gun == model.BaslangicTarihi.DayOfWeek);
                if (gucret == null)
                    ucret = GetirDefaultKortUcreti();
                else
                    ucret = gucret.Ucret;

                ucret = ucret * model.Seans;
                Odeme odeme = new Odeme()
                {
                    OdenecekTutar = ucret,
                    OdendiMi = !model.oyunSonrasiOdenecek
                };

                Rezervasyon rez = new Rezervasyon();

                var kullanici = GetLoginedUser();
                if (kullanici != null)
                {
                    rez.FKUyeID = kullanici.Id;
                    model.Ad = kullanici.Ad;
                    model.Soyad = kullanici.Soyad;
                    model.TelNo = kullanici.TelNo;
                }
                else
                {
                    rez.Ad = model.Ad;
                    rez.Soyad = model.Soyad;
                    rez.TelNo = model.TelNo;
                }

                rez.BaslangicTarihi = model.BaslangicTarihi.AddHours(model.BaslangicSaati);
                rez.BitisTarihi = rez.BaslangicTarihi.AddHours(model.Seans);
                rez.FKKortID = model.FKKortID;

                var baslangicTarihi = model.BaslangicTarihi.AddHours(model.BaslangicSaati);
                if (db.Rezervasyonlar.Any(x=>x.BaslangicTarihi== baslangicTarihi))
                {
                    odeme.OdendiMi = false;
                    model.oyunSonrasiOdenecek = true;
                    rez.EnumRezervasyonDurumu = EnumRezervasyonDurumu.Beklemede;
                    model.Bilgi = "Rezervasyon yapmak istediğiniz gün ve saatte daha önceden alınan rezervasyon olduğu için rezervasyonunuzu beklemeye aldık";
                }


                db.Odeme.Add(odeme);
                db.SaveChanges();

                rez.FKOdemeID = odeme.ID;
                
                db.Rezervasyonlar.Add(rez);
                db.SaveChanges();

                var kort = db.Kortlar.Find(rez.FKKortID);
                model.KortAdi = kort.KortAdi;
                model.Bilgi = string.IsNullOrEmpty(model.Bilgi) ? "Rezervasyonunuz Oluşturuldu" : model.Bilgi;



            }
            return View("RezervasyonSonuc",model);
        }

        public ActionResult OdemeSecimi(int kortid, int seans, DateTime baslangicTarihi)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                double ucret;
                var gucret = db.KortGunlukUcretler.FirstOrDefault(x => x.FKKortID == kortid && x.Gun ==baslangicTarihi.DayOfWeek);
                if (gucret == null)
                    ucret = GetirDefaultKortUcreti();
                else
                    ucret = gucret.Ucret;

                return View(ucret*seans);
                
            }
        }
    }
}