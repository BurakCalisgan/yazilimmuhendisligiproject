﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KortKiralamaOdevi.Models;
using KortKiralamaOdevi.Models.dbEntities;

namespace KortKiralamaOdevi.Areas.admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class KortGunlukUcretleriController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: admin/KortGunlukUcretleri
        public ActionResult Index()
        {
            var kortGunlukUcretler = db.KortGunlukUcretler.Include(k => k.Kort);
            return View(kortGunlukUcretler.ToList());
        }

        // GET: admin/KortGunlukUcretleri/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KortGunlukUcret kortGunlukUcret = db.KortGunlukUcretler.Find(id);
            if (kortGunlukUcret == null)
            {
                return HttpNotFound();
            }
            return View(kortGunlukUcret);
        }

        // GET: admin/KortGunlukUcretleri/Create
        public ActionResult Create()
        {
            ViewBag.FKKortID = new SelectList(db.Kortlar, "ID", "KortAdi");
            return View();
        }

        // POST: admin/KortGunlukUcretleri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FKKortID,Gun,Ucret")] KortGunlukUcret kortGunlukUcret)
        {
            if (ModelState.IsValid)
            {
                db.KortGunlukUcretler.Add(kortGunlukUcret);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FKKortID = new SelectList(db.Kortlar, "ID", "KortAdi", kortGunlukUcret.FKKortID);
            return View(kortGunlukUcret);
        }

        // GET: admin/KortGunlukUcretleri/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KortGunlukUcret kortGunlukUcret = db.KortGunlukUcretler.Find(id);
            if (kortGunlukUcret == null)
            {
                return HttpNotFound();
            }
            ViewBag.FKKortID = new SelectList(db.Kortlar, "ID", "KortAdi", kortGunlukUcret.FKKortID);
            return View(kortGunlukUcret);
        }

        // POST: admin/KortGunlukUcretleri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FKKortID,Gun,Ucret")] KortGunlukUcret kortGunlukUcret)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kortGunlukUcret).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FKKortID = new SelectList(db.Kortlar, "ID", "KortAdi", kortGunlukUcret.FKKortID);
            return View(kortGunlukUcret);
        }

        // GET: admin/KortGunlukUcretleri/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KortGunlukUcret kortGunlukUcret = db.KortGunlukUcretler.Find(id);
            if (kortGunlukUcret == null)
            {
                return HttpNotFound();
            }
            return View(kortGunlukUcret);
        }

        // POST: admin/KortGunlukUcretleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KortGunlukUcret kortGunlukUcret = db.KortGunlukUcretler.Find(id);
            db.KortGunlukUcretler.Remove(kortGunlukUcret);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
