﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KortKiralamaOdevi.Models;
using KortKiralamaOdevi.Models.dbEntities;

namespace KortKiralamaOdevi.Areas.admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class KortlarController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: admin/Kortlar
        public ActionResult Index()
        {
            return View(db.Kortlar.ToList());
        }

        // GET: admin/Kortlar/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kort kort = db.Kortlar.Find(id);
            if (kort == null)
            {
                return HttpNotFound();
            }
            return View(kort);
        }

        // GET: admin/Kortlar/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/Kortlar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,KortAdi,ResimUrl")] Kort kort)
        {
            if (ModelState.IsValid)
            {
                db.Kortlar.Add(kort);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kort);
        }

        // GET: admin/Kortlar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kort kort = db.Kortlar.Find(id);
            if (kort == null)
            {
                return HttpNotFound();
            }
            return View(kort);
        }

        // POST: admin/Kortlar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,KortAdi,ResimUrl")] Kort kort)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kort).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kort);
        }

        // GET: admin/Kortlar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kort kort = db.Kortlar.Find(id);
            if (kort == null)
            {
                return HttpNotFound();
            }
            return View(kort);
        }

        // POST: admin/Kortlar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kort kort = db.Kortlar.Find(id);
            db.Kortlar.Remove(kort);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
