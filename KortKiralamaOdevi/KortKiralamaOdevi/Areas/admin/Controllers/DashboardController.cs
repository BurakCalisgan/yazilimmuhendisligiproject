﻿using System.Web.Mvc;
using KortKiralamaOdevi.Areas.admin.Models;
using KortKiralamaOdevi.Controllers;

namespace KortKiralamaOdevi.Areas.admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DashboardController : BaseController
    {
        DashboardBusiness dashboardBusiness = new DashboardBusiness();
        // GET: admin/Home
        public ActionResult Index()
        {
            var liste = dashboardBusiness.GetRezervationForDay();
            return View(liste);
        }
    }
}