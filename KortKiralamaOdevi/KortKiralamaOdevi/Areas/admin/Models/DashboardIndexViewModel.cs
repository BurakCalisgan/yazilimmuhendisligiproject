﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class DashboardIndexViewModel
    {
        [Display(Name = "Kort Adı")]
        public string kortAdi { get; set; }
        [Display(Name = "Rezervasyon Yapan Kişi")]
        public string rezervasyonYapanKullanici { get; set; }
        [Display(Name = "Telefon Numarası")]
        public string telNo { get; set; }
        [Display(Name = "Gün")]
        public DayOfWeek Gun { get; set; }
        [Display(Name = "Başlangıç Saati")]
        public DateTime baslangicSaati { get; set; }
        [Display(Name = "Bitiş Saati")]
        public DateTime bitisSaati { get; set; }
        [Display(Name = "Ödeme Tutarı")]
        public double odemeTutari { get; set; }
        [Display(Name = "Ödeme Durumu")]
        public bool odemeDurumu { get; set; }
    }
}