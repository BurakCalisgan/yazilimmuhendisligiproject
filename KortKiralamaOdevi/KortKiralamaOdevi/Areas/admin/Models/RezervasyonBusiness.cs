﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using KortKiralamaOdevi.Models;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class RezervasyonBusiness
    {
        public RezervasyonIndexViewModel GetirRezervasyonBilgileriIndexViewModel(int kortid)
        {
            RezervasyonIndexViewModel model =  new  RezervasyonIndexViewModel();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                model.KortAdi = db.Kortlar.Find(kortid).KortAdi;// bunlar hep hata dönebilir kontrol edilmeli 
            }
            model.RezervasyonViewModels = GetirHaftalikRezervasyonBilgileri(kortid);
            return model;
        }
        public List<RezervasyonViewModel> GetirHaftalikRezervasyonBilgileri(int kortid)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var bitisSuresi = DateTime.Now.AddDays(6);

                    var liste = db.Rezervasyonlar.Where(x =>
                        x.FKKortID == kortid && x.BaslangicTarihi >= DateTime.Now &&
                        x.BaslangicTarihi <= bitisSuresi).Select(x => new RezervasyonViewModel()
                        {
                            rezervasyonId = x.ID,
                            Baslangic = x.BaslangicTarihi,
                            Bitis = x.BitisTarihi,
                            RezervasyonYapanKullanici =
                            string.IsNullOrEmpty(x.FKUyeID)
                                ? (x.Ad + x.Soyad)
                                : (x.Uye.Ad + " " + x.Uye.Soyad),

                        }).ToList();
                    var aaa = db.Rezervasyonlar.ToList();
                    liste.ForEach(x =>
                    {
                        x.Gun = x.Baslangic.DayOfWeek;
                    });
                    if (liste.GroupBy(x => x.Gun).Select(x => x.Key).ToList()
                            .Intersect(Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>().ToList()).Count() != 7)
                    //eğer listedeki tüm elemanlar ile tüm günlerin kesişiminin sayısı 7 ise listede tüm günler vardır
                    //yok ise eksik gün için boş veri ekleyip ekranda o gününde gözükmesini sağlayacağız
                    {
                        var eslenenListe = liste.GroupBy(x => x.Gun).Select(x => x.Key).ToList()
                            .Intersect(Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>().ToList()).ToList();
                        foreach (var gun in Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>().ToList())
                        {
                            if (!eslenenListe.Contains(gun))
                            {
                                liste.Add(new RezervasyonViewModel()
                                {
                                    Gun = gun
                                });
                            }
                        }
                    }
                    return liste;
                }


            }
            catch (Exception e)
            {
                throw;
            }
        }

        public RezervasyonDetayViewModel GetirRezevasyonDetayBilgileri(int rezervasyonId)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    var liste = db.Rezervasyonlar.Where(x =>
                        x.ID == rezervasyonId).Select(x => new RezervasyonDetayViewModel()
                    {
                        rezervasyonId = x.ID,
                        Baslangic = x.BaslangicTarihi,
                        Bitis = x.BitisTarihi,
                        RezervasyonYapanKullanici =
                            string.IsNullOrEmpty(x.FKUyeID)
                                ? (x.Ad + x.Soyad)
                                : (x.Uye.Ad + " " + x.Uye.Soyad),
                        kortAdi = x.Kort.KortAdi,
                        TelNo = string.IsNullOrEmpty(x.FKUyeID)
                            ? x.TelNo
                            : x.Uye.TelNo,
                        odemeTutari = x.Odeme.OdenecekTutar,
                        rezervasyonDurumu = x.EnumRezervasyonDurumu,
                        odemeDurum = x.Odeme.OdendiMi
                    }).FirstOrDefault();
                    return liste;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}