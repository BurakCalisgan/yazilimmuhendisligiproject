﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class RezervasyonViewModel
    {
        public int rezervasyonId { get; set; }
        public string RezervasyonYapanKullanici { get; set; }
        public DateTime Baslangic { get; set; }
        public DateTime Bitis { get; set; }
        public DayOfWeek Gun { get; set; }
    }
}