﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class RezervasyonDetayViewModel
    {
        public int rezervasyonId { get; set; }
        [Display(Name = "Rezervasyon Yapan Kişi")]
        public string RezervasyonYapanKullanici { get; set; }
        [Display(Name = "Telefon")]
        public string TelNo { get; set; }
        [Display(Name = "Rezervasyon Başlangıç Tarihi")]
        public DateTime Baslangic { get; set; }
        [Display(Name = "Rezervasyon Bitiş Tarihi")]
        public DateTime Bitis { get; set; }
        [Display(Name = "Kort Adı")]
        public string kortAdi { get; set; }
        [Display(Name = "Ücret")]
        public double odemeTutari { get; set; }
        [Display(Name = "Rezervasyon durumu")]
        public EnumRezervasyonDurumu rezervasyonDurumu { get; set; }
        [Display(Name = "Ödeme Durumu")]
        public bool odemeDurum { get; set; }
    }
}