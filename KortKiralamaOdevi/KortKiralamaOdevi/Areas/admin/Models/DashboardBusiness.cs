﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KortKiralamaOdevi.Models;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class DashboardBusiness
    {
        public List<DashboardIndexViewModel> GetRezervationForDay()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var liste = db.Rezervasyonlar.Where(x =>
                        x.BaslangicTarihi >= DateTime.Now).Select(x => new DashboardIndexViewModel()
                        {
                            kortAdi = x.Kort.KortAdi,
                            rezervasyonYapanKullanici =
                              string.IsNullOrEmpty(x.FKUyeID)
                                  ? (x.Ad + x.Soyad)
                                  : (x.Uye.Ad + " " + x.Uye.Soyad),
                            telNo = string.IsNullOrEmpty(x.FKUyeID)
                                ? x.TelNo
                                : x.Uye.TelNo,
                            baslangicSaati = x.BaslangicTarihi,
                            odemeTutari = x.Odeme.OdenecekTutar,
                            odemeDurumu = x.Odeme.OdendiMi,
                        }).ToList().OrderBy(x => x.Gun).ThenBy(x => x.baslangicSaati).ToList();
                    liste.ForEach(x => x.Gun = x.baslangicSaati.DayOfWeek);

                    return liste;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}