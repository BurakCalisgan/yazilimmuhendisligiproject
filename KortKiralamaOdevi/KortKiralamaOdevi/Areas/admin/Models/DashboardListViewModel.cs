﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class DashboardListViewModel
    {
        public List<DashboardIndexViewModel> DashboardIndexViewModel { get; set; }
    }
}