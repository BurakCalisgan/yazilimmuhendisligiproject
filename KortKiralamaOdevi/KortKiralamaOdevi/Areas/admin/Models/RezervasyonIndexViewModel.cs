﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KortKiralamaOdevi.Models.dbEntities;

namespace KortKiralamaOdevi.Areas.admin.Models
{
    public class RezervasyonIndexViewModel
    {
        public List<RezervasyonViewModel> RezervasyonViewModels { get; set; }
        public string KortAdi { get; set; }
    }
}