namespace KortKiralamaOdevi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kort_ucret : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Korts", "ResimuUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Korts", "ResimuUrl");
        }
    }
}
