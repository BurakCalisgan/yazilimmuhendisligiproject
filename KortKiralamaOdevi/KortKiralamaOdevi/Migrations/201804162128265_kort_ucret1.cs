namespace KortKiralamaOdevi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kort_ucret1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Korts", "ResimUrl", c => c.String());
            DropColumn("dbo.Korts", "ResimuUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Korts", "ResimuUrl", c => c.String());
            DropColumn("dbo.Korts", "ResimUrl");
        }
    }
}
