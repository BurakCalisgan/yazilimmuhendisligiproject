using System.Globalization;
using System.Web.Configuration;
using KortKiralamaOdevi.Models;
using KortKiralamaOdevi.Models.dbEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KortKiralamaOdevi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KortKiralamaOdevi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
        public DateTime GetRandomDate(DateTime from, DateTime to, double rnd)
        {
            TimeSpan range = new TimeSpan(to.Ticks - from.Ticks);

            var retVal = from + new TimeSpan((long)(range.Ticks * rnd));
            if (retVal.Hour >= 9 && retVal.Hour <= 15)
                return retVal;


            return GetRandomDate(from, to, rnd);
        }

        protected override void Seed(KortKiralamaOdevi.Models.ApplicationDbContext context)
        {

            Random rnd = new Random();

            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "musa"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "m_demir96@hotmail.com", Email = "m_demir96@hotmail.com" ,Ad = "Musa",Soyad = "DEM�R",TelNo = "531 111 11 11"};

                manager.Create(user, "asd123");
                manager.AddToRole(user.Id, "Admin");
            }

            if (!context.Kortlar.Any())
            {
                for (int i = 0; i < 6; i++)
                {
                    Kort kort = new Kort();
                    kort.KortAdi = "Kort " + i;
                    kort.ResimUrl = "http://www.paysa.com.tr/resimler/tenis-kortu-2-1332167466.jpg";
                    context.Kortlar.Add(kort);

                    context.SaveChanges();

                    for (int j = 0; j < 7; j++)
                    {
                        KortGunlukUcret gunlukUcret = new KortGunlukUcret();
                        gunlukUcret.FKKortID = kort.ID;
                        gunlukUcret.Ucret = rnd.Next(5, 20);
                        gunlukUcret.Gun = (DayOfWeek)j;

                        context.KortGunlukUcretler.Add(gunlukUcret);
                    }
                    context.SaveChanges();
                }
            }
            var tumRezervasyonlar = context.Rezervasyonlar.ToList();
            foreach (var rezervasyon in tumRezervasyonlar)
                context.Rezervasyonlar.Remove(rezervasyon);

            var tumOdemeler = context.Odeme.ToList();
            foreach (var odeme in tumOdemeler)
                context.Odeme.Remove(odeme);


            context.SaveChanges();
            var userId = context.Users.FirstOrDefault().Id;
            var kortId = context.Kortlar.FirstOrDefault().ID;
            for (int i = 0; i < 7; i++)
            {
                Odeme odeme = new Odeme();
                odeme.OdenecekTutar = rnd.Next(0, 12);
                odeme.OdendiMi = rnd.Next(0, 999) % 2 == 1;
                context.Odeme.Add(odeme);
                context.SaveChanges();


                Rezervasyon rez = new Rezervasyon();
                DateTime baslangic = DateTime.Now.AddDays(i);

                TimeSpan ts = new TimeSpan(rnd.Next(9, 16), rnd.Next(0, 999) % 2 == 1 ? 30 : 0, 0);
                baslangic = baslangic.Date + ts;
                rez.BaslangicTarihi = baslangic;
                rez.BitisTarihi = rez.BaslangicTarihi.AddHours(1);

                rez.FKUyeID = userId;
                rez.FKOdemeID = odeme.ID;
                rez.EnumRezervasyonDurumu = EnumRezervasyonDurumu.Aktif;
                rez.FKKortID = kortId;
                context.Rezervasyonlar.Add(rez);
                context.SaveChanges();

            }
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
