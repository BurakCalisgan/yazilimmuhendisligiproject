namespace KortKiralamaOdevi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kort_gunluk_ucret : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KortGunlukUcrets", "Ucret", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.KortGunlukUcrets", "Ucret");
        }
    }
}
