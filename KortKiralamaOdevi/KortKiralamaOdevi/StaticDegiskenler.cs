﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace KortKiralamaOdevi
{
    public class StaticDegiskenler
    {
        public static bool KullaniciGirisYaptiMi
        {
            get
            {
                var kullanici = System.Web.HttpContext.Current.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>()
                    .FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                return kullanici != null;
            }
        }
    }
}