﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Models
{
    public class AnasayfaViewModel
    {
        public int ID { get; set; }
        public string KortAdi { get; set; }
        public string ResimUrl { get; set; }
        public double Ucret { get; set; }
    }
}