﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Models
{
    public class RezervasyonYapViewModel
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }

        public string TelNo { get; set; }
        public DateTime BaslangicTarihi { get; set; }
        public int BaslangicSaati { get; set; }
        public int Seans { get; set; }
        public int FKKortID { get; set; }
        public string KortAdi { get; set; }
        public bool oyunSonrasiOdenecek { get; set; }

        public string Bilgi { get; set; }

    }
}