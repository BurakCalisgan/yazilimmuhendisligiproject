﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Models
{
    public class KullaniciBilgileriViewModel
    {
        public string AdSoyad { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string KullaniciAdi { get; set; }
        public string Role { get; set; }
    }
}