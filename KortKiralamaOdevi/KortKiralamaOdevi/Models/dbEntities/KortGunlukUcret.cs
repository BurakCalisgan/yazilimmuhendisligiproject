﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Models.dbEntities
{
    public class KortGunlukUcret
    {
        public int ID { get; set; }
        [Display(Name = "Kort")]
        public int FKKortID { get; set; }
        [Display(Name = "Gün")]
        public DayOfWeek Gun { get; set; }
        [Display(Name = "Saatlik Ücret")]
        public double Ucret { get; set; }

        [ForeignKey("FKKortID")]
        [Display(Name = "Kort")]
        public virtual Kort Kort { get; set; }
    }
}