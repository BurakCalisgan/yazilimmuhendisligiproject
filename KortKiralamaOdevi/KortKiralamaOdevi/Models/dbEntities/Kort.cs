﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KortKiralamaOdevi.Models.dbEntities
{
    public class Kort
    {
        public Kort()
        {
            GunlukKortUcretleri = new HashSet<KortGunlukUcret>();
        }
        public int ID { get; set; }
        [Display(Name = "Kort Adı")]
        public string KortAdi { get; set; }
        [Display(Name = "Kort Resmi")]
        public string ResimUrl { get; set; }
        public virtual ICollection<KortGunlukUcret> GunlukKortUcretleri { get; set; }
    }
}